<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Slider;
use App\Models\Category;


class HomeController extends Controller
{
    
    // base function for admin 
    public function index()
    {
        $data['slider'] = Slider::select('name','description','img','link')
        ->where('status','yes')
        ->where('language_id',lang_front())->take(3)->get();
        
        return view('front.index')->with($data);
    }



    // base function for admin 
    public function about()
    {
        
        return view('front.static.about');
    }

}
