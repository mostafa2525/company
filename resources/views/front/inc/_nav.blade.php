<!------ Header Start ------>
<nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full"> 
  
  <!-- Start Top Search -->
  <div class="top-search">
    <div class="container">
      <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span>
        <input type="text" class="form-control" placeholder="Search">
        <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> </div>
    </div>
  </div>
  <!-- End Top Search -->
  
  <div class="container"> 
    <!-- Start Atribute Navigation -->
   
    <!-- End Atribute Navigation --> 
    
    <!-- Start Header Navigation -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="fa fa-bars"></i> </button>
      <a href="{{route('front.get.home.index')}}" class="logo navbar-brand"> 
      <img class="logo logo-display" src="{{getImage(SETTINGS_PATH.$setting->logo)}}" alt="{{$setting->site_name}}"> 
      <img class="logo logo-scrolled" src="{{getImage(SETTINGS_PATH.$setting->logo)}}" alt="{{$setting->site_name}}"> 
      </a> 
      </div>
    <!-- End Header Navigation --> 
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-menu">
      <ul class="nav navbar-nav navbar-right" data-in="fadeIn" data-out="fadeOut">
      

      <li> 
        <a href="{{route('front.get.home.index')}}" class="" >{{ json_data($site_content,'sectionLinks_home') }}</a>
      </li>
      <li> 
        <a href="{{route('front.get.home.about')}}" class="" >{{ json_data($site_content,'sectionLinks_about') }}</a>
      </li>
      
        <li class="dropdown"> <a href="{{route('front.get.service.all')}}" class="dropdown-toggle" data-toggle="dropdown">{{ json_data($site_content,'sectionLinks_services') }}</a>
          <ul class="dropdown-menu">
          @foreach($services as $ser)
            <li>
            <a href="{{route('front.get.service.show',[$ser->slug])}}">
              {{\Str::Words($ser->name,'3',' ')}}
            </a>
            </li>
          @endforeach
          </ul>
        </li>

      <li> 
        <a href="{{route('front.get.contactus.gallery')}}" class="" >{{ json_data($site_content,'sectionLinks_gallery') }}</a>
      </li>


      <li> 
        <a href="{{route('front.get.blog.index')}}" class="" >{{ json_data($site_content,'sectionLinks_blog') }}</a>
      </li>

      <!-- <li> 
        <a href="{{--route('front.get.portfolio.index')--}}" class="" >Portofolio</a>
      </li> -->

      <li> 
        <a href="{{route('front.get.contactus.contact')}}" class="" >{{ json_data($site_content,'sectionLinks_contactUs') }}</a>
      </li>

  
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
</nav>
<!------ Header End ------>  