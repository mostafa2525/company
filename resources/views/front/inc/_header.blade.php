<!DOCTYPE html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<!-- Mirrored from www.incognitothemes.com/alpha/media-advertising.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 25 Mar 2019 13:19:56 GMT -->
<head>
<meta charset="utf-8">
<meta name="google" content="notranslate">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{$setting->site_name}} 
  @yield('title')
</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="{{furl()}}/assets/images/log.png">
<link rel="stylesheet" href="{{furl()}}/assets/css/master.css">
<link rel="stylesheet" href="{{furl()}}/assets/css/responsive.css">
<link rel="stylesheet" href="{{furl()}}/seoera/css/ltr.css">
</head>
<body>
<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your eiterience.</p>
<![endif]--> 

<!------ Loader Start ------>
<div id="loader-overlay">
  <div class="loader-wrapper">
    <div class="alpha-dots">
      <div class="alpha-child alpha-dot1"></div>
      <div class="alpha-child alpha-dot2"></div>
    </div>
  </div>
</div>
<!------ Loader End ------> 