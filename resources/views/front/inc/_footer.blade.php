

<!------ Clients Start ------>

<section class="pt-80 pb-80 white-bg">
  <div class="container-fluid">
    <div class="row">
      <div id="client-slider" class="owl-carousel">
      @foreach($brands as $br)
        <div class="client-logo">
          <img class="img-responsive" src="{{getImage(BRAND_PATH.'small/'.$br->img)}}" alt="{{$br->name}}" 
           style="height:100px; width:200px;"/>
        </div>
      @endforeach
      </div>
    </div>
  </div>
</section>

<!------ Clients End ------> 




<!------ Footer Start ------>
<footer class="footer">
  <div class="footer-main">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-md-4">
          <div class="widget widget-text">
            <h5 class="widget-title">{{ json_data($site_content,'footer_head1') }}</h5>
            <p>{!! json_data($site_content,'footer_desc') !!}</p>
          </div>
        </div>
        <div class="col-sm-6 col-md-2">
          <div class="widget widget-links">
            <h5 class="widget-title"> {{ json_data($site_content,'footer_head2') }} </h5>
            <ul>
              @foreach($services as $ser)
              <li>
                <a href="{{route('front.get.service.show',[$ser->slug])}}">
                  {{\Str::Words($ser->name,'3',' ')}}
                </a>
              </li>
              @endforeach
              
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-2">
          <div class="widget widget-links">
            <h5 class="widget-title">{{ json_data($site_content,'footer_head3') }}</h5>
            <ul>
              <li><a href="{{route('front.get.home.index')}}">{{ json_data($site_content,'sectionLinks_home') }}</a></li>
              <li><a href="{{route('front.get.home.about')}}">{{ json_data($site_content,'sectionLinks_about') }}</a></li>
              <li><a href="{{route('front.get.service.all')}}">{{ json_data($site_content,'sectionLinks_services') }}</a></li>
              <li><a href="{{route('front.get.contactus.gallery')}}">{{ json_data($site_content,'sectionLinks_gallery') }}</a></li>
              <li><a href="{{route('front.get.blog.index')}}">{{ json_data($site_content,'sectionLinks_blog') }}</a></li>
              <li><a href="{{route('front.get.contactus.contact')}}"> {{ json_data($site_content,'sectionLinks_contactUs') }} </a></li>

            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="widget widget-text">
            <h5 class="widget-title">{{ json_data($site_content,'footer_head4') }}</h5>
            <ul class="footer-gallery" id="footer-gallery">

            @foreach($services as $ser)
              <li>
                <div class="footer-gallery-box">
                  <div class="skin-overlay"></div>
                  <img src="{{getImage(SERVICE_PATH.'small/'.$ser->img)}}" alt="{{$ser->name}}" style="height:60px; width:85px;">
                  <div class="zoom-wrap text-center">
                    <ul class="footer-gallery-zoom">
                      <li><a class="alpha-lightbox" href="{{getImage(SERVICE_PATH.'medium/'.$ser->img)}}"><i class="fa fa-search"></i></a></li>
                    </ul>
                  </div>
                </div>
              </li>
            @endforeach
            
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-xs-12">
          <div class="copy-right">© 2019 <a href="https://eraasoft.com/" target="_blank"> eraasoft </a> </div>
        </div>
        <div class="col-md-6 col-xs-12">
          <ul class="social-media">
            @if($setting->facebook)
              <li><a href="{{$setting->facebook}}" target="_blank" class="fa fa-facebook"></a></li>
            @endif

            @if($setting->twitter)
              <li><a href="{{$setting->twitter}}"  target="_blank" class="fa fa-twitter"></a></li>
            @endif

            @if($setting->pinterest)
              <li><a href="{{$setting->pinterest}}" target="_blank" class="fa fa-pinterest"></a></li>
            @endif

            @if($setting->instagram)
              <li><a href="{{$setting->instagram}}" target="_blank" class="fa fa-instagram"></a></li>
            @endif

            @if($setting->linkedin)
              <li><a href="{{$setting->linkedin}}" target="_blank" class="fa fa-linkedin-square"></a></li>
            @endif
 
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
<!------ Footer End ------> 

<!-- GO TO TOP  --> 
<a href="#" id="back-to-top" title="Back to top">&uarr;</a> 
<!-- GO TO TOP End --> 