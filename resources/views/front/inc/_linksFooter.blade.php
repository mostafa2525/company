

<!------ Javascript Plugins ------> 
<script src="{{furl()}}/assets/js/jquery.min.js"></script>
<script src="{{furl()}}/assets/js/validator.js"></script>
<script src="{{furl()}}/assets/js/plugins.js"></script>
<script src="{{furl()}}/assets/js/master.js"></script> 
<script src="{{furl()}}/assets/js/bootsnav.js"></script> 
<script type="text/javascript" src="{{furl()}}/seoera/toster/jquery.toast.min.js"></script>

<script type="text/javascript" src="{{furl()}}/seoera/parsley.js"></script>
@if(Request::segment(1) == 'ar')
<script type="text/javascript" src="{{furl()}}/seoera/i18n/ar.js"></script>
@else
<script type="text/javascript" src="{{furl()}}/seoera/i18n/en.js"></script>
@endif




@if(session('message') !=  null )
  <script type="text/javascript">
    $.toast({
                heading: 'Message',
                text: "{{ session('message') }}",
                position: 'top-right',
                stack: false,
                icon: 'info',
                hideAfter: 10000 
            })
  </script>
@endif

@yield('script')

<!------ Javascript Plugins End ------>
</body>

<!-- Mirrored from www.incognitothemes.com/alpha/media-advertising.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 25 Mar 2019 13:25:17 GMT -->
</html>