@extends('front.main')


@section('content')
<!------ Flex Slider Start ------>
<section class="pt-0 pb-0">
  <div class="slider-bg flexslider">
    <ul class="slides">
      


      @foreach($slider as $sl)
      <!-- SLIDE 1 -->
      <li>
        <div class="slide-img" style="background:url({{getImage(SLIDER_PATH.$sl->img)}}) center center / cover scroll no-repeat;"></div>
        <div class="hero-text-wrap">
          <div class="hero-text white-color">
            <div class="container text-center">
              <h2 class="white-color">{{$sl->name}}</h2>
              <h3 class="white-color raleway-font mt-30">{{$sl->description}}</h3>
              @if($sl->link)
              <p class="text-center mt-30">
                <a class="btn btn-outline-white btn-circle">@lang('frontSite.viewMore')</a> 
              </p>
              @endif
            </div>
          </div>
        </div>
      </li>
      @endforeach
      
   
      
    </ul>
  </div>
</section>
<!------ Flex Slider End ------>

<!------ Who We Are Start ------>
<section>
        <div class="clearfix">
          <div class="about-us-bg bg-flex col-md-6"></div>
          <div class="col-about-right col-md-6 col-md-offset-6">
            <h2>{{ json_data($site_content,'section1_head') }}</h2>
        	<h4 class="mt-10 droid-font font-300"> {{ json_data($site_content,'section1_small_desc') }}    </h4>
            <div class="mt-30">
          	<p>{!! json_data($site_content,'section1_desc') !!} </p>
        	</div>
            
            <div class="mt-30">
          		<a class="btn btn-color btn-circle btn-animate" href="{{route('front.get.home.about')}}">
                <span> {{ json_data($site_content,'sectionLinks_about') }} <i class="fa fa-rocket"></i></span>
              </a>
        	</div>
                        
          </div>
        </div>
</section>
<!------ Who We Are End ------> 





<!------ Portfolio Start ------>
<section class="pt-0 pb-0">
  <div class="container-fluid">

    <div class="row">
      <div class="col-sm-8 section-heading" >
        <h2 style="margin-top:70px;">{{ json_data($site_content,'section2_head') }} </h2>
        <h4 class="mt-10 droid-font font-300" style="margin-bottom:50px;">{{ json_data($site_content,'section2_desc') }}  </h4>
      </div>
    </div>


    <div class="row">
      <div class="portfolio-container text-center">
        <ul id="portfolio-grid" class="four-column hover-two">


        @foreach($services as $ser)
          <li class="portfolio-item">
            <div class="portfolio">
              <div class="dark-overlay"></div>
              <img src="{{getImage(SERVICE_PATH.'small/'.$ser->img)}}" alt="{{$ser->name}}" style="height:225px;">
              <div class="portfolio-wrap">
                <div class="portfolio-description">
                  <h3 class="portfolio-title">{{\Str::Words($ser->name,'3','..')}}</h3>
                  <a href="{{route('front.get.service.category',[$ser->category->slug])}}" class="links"> {{\Str::Words($ser->category->name,'3','..')}} </a> </div>
                <!-- /.project-info -->
                
                <ul class="portfolio-details">
                  <li><a class="alpha-lightbox" href="{{getImage(SERVICE_PATH.'medium/'.$ser->img)}}"><i class="fa fa-search"></i></a></li>
                  <li><a href="{{route('front.get.service.show',[$ser->slug])}}"><i class="fa fa-link"></i></a></li>
                </ul>
              </div>
            </div>
            <!-- /.portfolio --> 
          </li>
        @endforeach
         
        </ul>
      </div>
    </div>
  </div>
</section>
<!------ Portfolio End ------>




<!------ Tabs Icon Start ------>
<section class="white-bg">
  <div class="container">
  	<div class="row">
		<div class="col-sm-8 section-heading">
            <h2>  {{ json_data($site_content,'section3_head') }}   </h2>
            <h4 class="mt-10 droid-font font-300 line-height-30">
            {{ json_data($site_content,'section3_desc') }}
            </h4>
      	</div>
    </div>
    <div class="row mt-40">
      <div class="col-md-8 col-md-offset-2">
        <div class="icon-tabs"> 
          
          <!-- Nav tabs -->
          <ul class="nav nav-tabs text-center" role="tablist">
            <li role="presentation" class="active">
              <a href="#design" role="tab" data-toggle="tab">
              {!! json_data($site_content,'section3_titleIcon1') !!} {{json_data($site_content,'section3_icon1')}} 
              </a>
            </li>
            <li role="presentation">
              <a href="#development" role="tab" data-toggle="tab">
              {!! json_data($site_content,'section3_titleIcon2') !!} {{json_data($site_content,'section3_icon2')}}
            </a>
            </li>
            <li role="presentation">
              <a href="#apps" role="tab" data-toggle="tab">
              {!! json_data($site_content,'section3_titleIcon3') !!} {{json_data($site_content,'section3_icon3')}}
              </a>
          </li>
            <li role="presentation">
              <a href="#seo" role="tab" data-toggle="tab">
              {!! json_data($site_content,'section3_titleIcon4') !!} {{json_data($site_content,'section3_icon4')}}
              </a>
              </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content text-center">
            <div role="tabpanel" class="tab-pane fade in active" id="design">
              <p>{!! json_data($site_content,'section3_descIcon1') !!}</p>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="development">
            <p>{!! json_data($site_content,'section3_descIcon2') !!}</p>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="apps">
            <p>{!! json_data($site_content,'section3_descIcon3') !!}</p>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="seo">
            <p>{!! json_data($site_content,'section3_descIcon4') !!}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!------ Tabs Icon End ------>

 
<!------ Testimonails Start ------>
<section class="parallax-bg-4 fixed-bg" data-stellar-background-ratio="0.2">
  <div class="overlay-bg-dark"></div>
  <div class="container">
  	<div class="row">
      <div class="col-sm-8 section-heading white-color">
        <h2> {{ json_data($site_content,'section4_head') }}  </h2>
        <h4 class="mt-10 droid-font font-300"> {{ json_data($site_content,'section4_desc') }}   </h4>
      </div>
    </div>
    <div class="row mt-50">
      <div class="owl-carousel testimonial"> 
        
        <!-- Slide -->
        <div class="testimonial-item"> <img class="img-responsive img-circle" src="{{furl()}}/assets/images/team/avatar-1.jpg" alt="avatar-1"/>
          <h4>Jazz’s has an entire page dedicated to why customers have chosen them. It’s clean and simple, featuring customers reviews in a “spotlight” format.</h4>
          <h5 class="montserrat-font upper-case">Jazz Smith</h5>
          <p>Web Designer</p>
        </div>
        
        <!-- Slide -->
        <div class="testimonial-item"> <img class="img-responsive img-circle" src="{{furl()}}/assets/images/team/avatar-2.jpg" alt="avatar-2"/>
          <h4>Friendly, Helpful and treated us like top customers. Thank you Doteasy! We have hosted through Doteasy for years, and this type of customer service is part of what will keep us here.</h4>
          <h5 class="montserrat-font upper-case">Shawn Smith</h5>
          <p>Art Direction</p>
        </div>
        
        <!-- Slide -->
        <div class="testimonial-item"> <img class="img-responsive img-circle" src="{{furl()}}/assets/images/team/avatar-3.jpg" alt="avatar-3"/>
          <h4>Everything I had asked for and more in my web site, and much help from Customer Service. My husband and I greatly appreciated Ken's service to us. Thank you very much.</h4>
          <h5 class="montserrat-font upper-case">Susan Smith</h5>
          <p>App Developer</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!------ Testimonails End ------>


<!------ Contact Us Start ------>

<section class="contact-us" id="contact">
  <div class="container">
  	<div class="row">
      <div class="col-sm-8 section-heading">
        <h2>{{ json_data($site_content,'section5_head') }}  </h2>
        <h4 class="mt-10 droid-font font-300">{{ json_data($site_content,'section5_desc') }} </h4>
      </div>
    </div>
    <div class="row mt-50">
      <div class="col-sm-12 col-md-offset-2 col-md-8">
        <form  class="contact-me" id="contact-form"  >
          @csrf
          <div class="col-md-12">
            <ul id="errors"></ul>
          </div>
          <div class="messages"></div>
          <div class="form-group">
            <label class="sr-only" for="name">Name</label>
            <input type="text" name="name" class="form-control" id="name" required="required" placeholder="Your Name" data-error="Your Name is Required">
            <div class="help-block with-errors mt-20"></div>
          </div>

          <div class="form-group">
            <label class="sr-only" for="email">Email</label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Your Email" required="required" data-error="Please Enter Valid Email">
            <div class="help-block with-errors mt-20"></div>
          </div>
          <div class="form-group">
            <label class="sr-only" for="phone">Phone</label>
            <input type="phone" name="phone" class="form-control" id="phone" placeholder="Phone" required>
          </div>
          <div class="form-group">
            <label class="sr-only" for="enquiry">Enquiry</label>
            <textarea name="message" class="form-control" id="message" rows="7" placeholder="Your Message" required data-error="Please, Leave us a message"></textarea>
            <div class="help-block with-errors mt-20"></div>
          </div>
          <p class="text-center"><button type="submit" name="submit" class="btn btn-color btn-circle">Send Request <i class="mdi mdi-email"></i></button></p>
        </form>
      </div>
    </div>
  </div>
</section>

<!------ Contact Us End ------>


@endsection




@section('script')

<script type="text/javascript">
    // $('#contact-form').parsley();
  </script>

<script type="text/javascript">
  


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#contact-form").submit(function(e) 
    {
        // e.preventDefault();
        var formData  = new FormData(jQuery('#contact-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.contactus.sendMessage')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.form-group').find("input,textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection