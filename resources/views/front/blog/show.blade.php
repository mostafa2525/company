@extends('front.main')


@section('content')

<!-- page-title-section start -->
<section class="title-hero-bg blog-cover-bg" data-stellar-background-ratio="0.2">
  <div class="container">
    <div class="page-title text-center">
      <h1>  {{$row->name}} </h1>
    </div>
  </div>
</section>
<!-- page-title-section end --> 

<!------ Blogs Start ------>
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="post">
          <div class="post-img"> <img class="img-responsive" src="{{getImage(BLOG_PATH.$row->img)}}" alt=""/> </div>
          <div class="post-info">
            <h3>{{$row->name}}</h3>
            <h6>December 26, 2016</h6>
            <p></p>
        </div>
        
        
        <div class="blog-standard">
        	<p>{!! $row->description !!}</p>
                
              </div>
              <div class="post-tags m-5 ">
                <a href="#">Design</a>
                <a href="#">Branding</a>
                <a href="#">Stationery</a>
                <a href="#">Development</a>
                <a href="#">Concept</a>
              </div>

          </div>
      </div>
      
      <!-- Left Side End-->
      
      <div class="col-md-3 col-md-offset-1 right-col-rv">
          
            <div class="widget widget_about">
              <h4 class="widget-title">About</h4>
              <p>{{$row->small_description}}</p>
            </div>

            <div class="widget sidebar_widget widget_categories">
              <h4 class="widget-title">Latest Posts</h4>
              <ul>
                @foreach($latest as $la)
                <li> <a href="{{route('front.get.blog.show',[$la->slug])}}">{{\Str::Words($la->name,'3','..')}}</a> </li>
                @endforeach
              </ul>
            </div>
            <div class="widget sidebar_widget widget_tag_cloud">
              <h4 class="widget-title">Tags</h4>
              <div class="post-tags">
                <a href="#">Design</a>
                <a href="#">Envato</a>
                <a href="#">Photography</a>
                <a href="#">Videos</a>
                <a href="#">Creative</a>
                <a href="#">Apps</a>
              </div>
            </div>
          </div>
          
          <!-- Right Side End -->

      
    </div>

  </div>
</section>
<!------ Blogs End ------> 
@endsection