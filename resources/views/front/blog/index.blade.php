@extends('front.main')


@section('content')


<!-- page-title-section start -->
<section class="title-hero-bg blog-cover-bg" data-stellar-background-ratio="0.2">
  <div class="container">
    <div class="page-title text-center">
      <h1>{{ json_data($site_content,'sectionLinks_blog') }}</h1>
    </div>
  </div>
</section>
<!-- page-title-section end --> 

<!------ Blogs Start ------>
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="row">

        @foreach($posts as $po)
          <div class="col-md-6">
            <div class="post">
              <div class="post-img"> <img class="img-responsive" src="{{getImage(BLOG_PATH.'small/'.$po->img)}}" alt=""/> </div>
                <div class="post-info">
                  <h3><a href="{{route('front.get.blog.show',[$po->slug])}}"> {{\Str::Words($po->name,'3','..')}}  </a></h3>
                  <h6>{{$po->created_at}}</h6>
                  <p>{{\Str::Words($po->small_description,'30','..')}} </p>
                  <a class="readmore dark-color" href="{{route('front.get.blog.show',[$po->slug])}}">
                    <span>@lang('frontSite.viewMore')</span> <i class="fa fa-angle-right"></i> </a> 
                  </div>
            </div>
            <!-- Post End -->
          </div>
        @endforeach


        </div>
        <div class="clearfix">
          {{$posts->links()}}
        </div>
      </div>
      
      <!-- Left Side End-->
      
      <div class="col-md-3 col-md-offset-1 right-col-rv">

        <div class="widget widget_about">
          <h4 class="widget-title">{{ json_data($site_content,'blog_title') }} </h4>
          <p>{!! json_data($site_content,'blog_desc') !!} </p>
        </div>
      
        <div class="widget sidebar_widget widget_categories">
          <h4 class="widget-title">Latest Posts</h4>
          <ul>
            @foreach($latest as $la)
            <li> <a href="{{route('front.get.blog.show',[$la->slug])}}">{{\Str::Words($la->name,'3','..')}}</a> </li>
            @endforeach
          </ul>
        </div>
        <div class="widget sidebar_widget widget_tag_cloud">
          <h4 class="widget-title">Tags</h4>
          <div class="post-tags"> <a href="#">Design</a> <a href="#">Envato</a> <a href="#">Photography</a> <a href="#">Videos</a> <a href="#">Creative</a> <a href="#">Apps</a> </div>
        </div>
      </div>
      
      <!-- Right Side End --> 
      
    </div>
  </div>
</section>
<!------ Blogs End ------> 



@endsection