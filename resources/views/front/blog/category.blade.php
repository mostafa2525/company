@extends('front.main')


@section('content')


<!-- page-title-section start -->
<section class="title-hero-bg blog-cover-bg" data-stellar-background-ratio="0.2">
  <div class="container">
    <div class="page-title text-center">
      <h1> {{$row->name}}  </h1>
    </div>
  </div>
</section>
<!-- page-title-section end --> 

<!------ Blogs Start ------>
<section class="pt-100 pb-100">
  <div class="container">

    <div class="row mt-50">

        @foreach($catService as $ser)
        <div class="col-md-4">
            <div class="post">
            <div class="post-img"> 
                <a href="{{route('front.get.service.show',[$ser->slug])}}" > 
                <img class="img-responsive" src="{{getImage(SERVICE_PATH.'small/'.$ser->img)}}" alt=""/> 
                </a>
            </div>
            <div class="post-info">
                <h3><a href="{{route('front.get.service.show',[$ser->slug])}}"> {{\Str::Words($ser->name,'4','..')}} </a></h3>
                <p> {{\Str::Words($ser->small_description,'20','...')}} </p>
                <a class="readmore dark-color" href="{{route('front.get.service.show',[$ser->slug])}}">
                    <span>@lang('frontSite.viewMore')</span>
                </a> 
                </div>
            </div>
        </div>
        @endforeach
      <!-- Post End -->
      
    </div>
  </div>
</section>
<!------ Blogs End ------> 






@endsection