@extends('front.main')


@section('content')

<!-- page-title-section start -->
<section class="title-hero-bg portfolio-cover-bg" data-stellar-background-ratio="0.2">
	<div class="container">
    	<div class="page-title text-center">
        	<h1>  {{$row->name}} </h1>
        </div>
	</div>
</section>
<!-- page-title-section end -->
        
<!------ Portfolio Start ------>
<section class="pt-100 pt-100">
  <div class="container">
    <div class="row">
		<div class="col-md-12 portfolio-left">
        	<div class="portfolio-slider owl-carousel">
                <div class="item text-center"><img  src="{{getImage(SERVICE_PATH.$row->img)}}" alt="{{$row->name}}"/></div>
                @foreach($row->images as $img)
                <div class="item"><img class="img-responsive" src="{{getImage(SERVICE_IMAGES_PATH.$img->img)}}" alt="{{$row->name}}"/></div>
                @endforeach
            </div>
            <h3>@lang('frontSite.serviceDetails')</h3>
            <p class="mt-20">{!! $row->description !!}</p>
            <p class="mt-50"><a class="btn btn-color btn-circle" href="{{route('front.get.contactus.contact')}}" >@lang('frontSite.startService')</a></p>
        </div>
        <!-- Left Side End -->
    </div>
  </div>
</section>
<!------ Portfolio End ------> 


@endsection