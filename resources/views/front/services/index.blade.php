@extends('front.main')


@section('content')


<!-- page-title-section start -->
<section class="title-hero-bg portfolio-cover-bg" data-stellar-background-ratio="0.2">
	<div class="container">
    	<div class="page-title text-center">
        	<h1> {{ json_data($site_content,'sectionLinks_services') }} </h1>
        </div>
	</div>
</section>
<!-- page-title-section end -->
        
<!------ Portfolio Start ------>
<section class="pt-100 pt-100">
  <div class="container">
    <div class="row">
      <div class="portfolio-container text-center">
        <ul id="portfolio-filter" class="list-inline filter-transparent">
          <li class="active" data-group="all">All</li>
          @foreach($categories as $cat)
          <li data-group="{{$cat->slug}}">{{$cat->name}}</li>
          @endforeach
        </ul>
        <ul id="portfolio-grid" class="four-column hover-two">


        @foreach($services as $ser)

          <li class="portfolio-item gutter-space" data-groups='["all", "{{$ser->category->slug}}"]'>
            <div class="portfolio">
              <div class="dark-overlay"></div>
              <img src="{{getImage(SERVICE_PATH.'small/'.$ser->img)}}" alt="">
              <div class="portfolio-wrap">
                <div class="portfolio-description">
                  <h3 class="portfolio-title">{{\Str::Words($ser->name,'3','..')}}</h3>
                  <a href="{{route('front.get.service.category',[$ser->category->slug])}}" class="links">
                    {{\Str::Words($ser->category->name,'3','..')}}
                  </a> 
                </div>
                <!-- /.project-info -->
                
                <ul class="portfolio-details">
                  <li><a class="alpha-lightbox" href="{{getImage(SERVICE_PATH.'medium/'.$ser->img)}}" ><i class="fa fa-search"></i></a></li>
                  <li><a href="{{route('front.get.service.show',[$ser->slug])}}"><i class="fa fa-link"></i></a></li>
                </ul>
              </div>
            </div>
            <!-- /.portfolio --> 
          </li>

        @endforeach

         
        </ul>
      </div>
    </div>
    <div class="row mt-100">
    	<p class="text-center">
            <a class="btn btn-color btn-circle" href="{{route('front.get.contactus.contact')}}">@lang('frontSite.startService')</a>
        </p>
    </div>
  </div>
</section>
<!------ Portfolio End ------> 





@endsection