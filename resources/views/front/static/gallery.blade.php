@extends('front.main')


@section('content')
<!-- page-title-section start -->
<section class="title-hero-bg widget-cover-bg" data-stellar-background-ratio="0.2">
  <div class="container">
    <div class="page-title text-center">
      <h1>{{ json_data($site_content,'sectionLinks_gallery') }}</h1>
    </div>
  </div>
</section>
<!-- page-title-section end --> 




<!------ Owl Slider Start ------>
<section class="pb-100">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 section-heading">
        <h2> Our Gallery </h2>
      </div>
    </div>
    <div class="row mt-50">
      <div class="col-md-12">
        <div class="owl-slider owl-carousel">


        @foreach($services as $ser)
          <div class="item" style="background:url({{getImage(SERVICE_PATH.$ser->img)}}) center center / cover scroll no-repeat;">
          		<div class="hero-text-wrap">
                <div class="hero-text">
                  <div class="container text-center">
                    <h2 class="white-color lora-font mt-30">{{$ser->name}}</h2>
                  </div>
                </div>
              </div>
          </div>
          @endforeach


        </div>
      </div>
    </div>
    
    
    
  </div>
</section>
<!------ Owl Slider End ------> 






@endsection

