@extends('front.main')


@section('content')

<!-- page-title-section start -->
<section class="title-hero-bg contact-cover-bg" data-stellar-background-ratio="0.2">
	<div class="container">
    	<div class="page-title text-center">
        	<h1>{{ json_data($site_content,'sectionLinks_contactUs') }} </h1>
        </div>
	</div>
</section>
<!-- page-title-section end -->


<!------ Contact Us Start ------>

<section class="contact-us">
  <div class="container">
    <div class="row mt-50">
      <div class="col-md-8">


        <form name="contact-form" id="contact-form" >
          <div class="messages"></div>
          	<div class="row">

              @csrf
                <div class="col-md-12">
                    <ul id="errors"></ul>
                </div>
            	
                    <div class="col-md-6">
                    	<div class="form-group">
                            <label class="sr-only" for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" required="required" placeholder="Your Name" data-error="Your Name is Required">
            				<div class="help-block with-errors mt-20"></div>
                        </div>
                    </div>
                
                
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="sr-only" for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="Your Email" required="required" data-error="Please Enter Valid Email">
            				<div class="help-block with-errors mt-20"></div>
                        </div>    
                    </div>
          </div>
          <div class="form-group">
            <label class="sr-only" for="phone">Phone</label>
            <input type="text" name="phone" class="form-control" id="phone" placeholder="Your Phone">
          </div>
          <div class="form-group">
            <label class="sr-only" for="message">Message</label>
            <textarea name="message" class="form-control" id="message" rows="7" placeholder="Your Message" required data-error="Please, Leave us a message"></textarea>
            <div class="help-block with-errors mt-20"></div>
          </div>
          <button type="submit" name="submit" class="btn btn-color btn-circle">Send Mail</button>
        </form>
      </div>
      <div class="col-md-4">
        <h3> Location</h3>
        <address>
        {!! $setting->address !!} <br>
        <abbr title="Phone">P:</abbr>  {{$setting->mobile}} <br>
        <a href="mailto:#">{{$setting->email}}</a>
        </address>
        <h3>{{ json_data($site_content,'contactUs_workH') }}</h3>
        <p> {!! json_data($site_content,'contactUs_workHContent') !!} </p>
      </div>
    </div>
  </div>
</section>

<!------ Contact Us End ------> 

@endsection



@section('script')

<script type="text/javascript">
    // $('#contact-form').parsley();
  </script>

<script type="text/javascript">
  


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#contact-form").submit(function(e) 
    {
        // e.preventDefault();
        var formData  = new FormData(jQuery('#contact-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.contactus.sendMessage')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.form-group').find("input,textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection


