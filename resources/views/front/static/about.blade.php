@extends('front.main')


@section('content')

<?php $aboutUs = json_decode($setting->who_us); ?>

<!-- page-title-section start -->
<section class="title-hero-bg about-cover-bg" data-stellar-background-ratio="0.2">
	<div class="container">
    	<div class="page-title text-center">
        	<h1> {{ json_data($site_content,'sectionLinks_about') }}</h1>
        </div>
	</div>
</section>
<!-- page-title-section end -->
        

<!------ Who We Are Start ------>
<section class="main-section">
  <div class="container">
  	<div class="row">
      <div class="col-sm-8 section-heading">
      	<div class="about-me">
   	    	<img class="" src="{{getImage(SETTINGS_PATH.'small/'.json_data($aboutUs,'img1'))}}" alt=""/> 
        </div>
        <h2>  {{json_data($aboutUs,'aboutUsTitle')}}   </h2>
        <h4 class="mt-10 raleway-font font-300"> {{json_data($aboutUs,'aboutUsSmallDescription')}}    </h4>
        <div class="mt-30">
          <p> {!! json_data($aboutUs,'aboutUsDescription') !!} </p>
        </div>
   
        <div class="mt-30">
        	<ul class="social-networks text-center">
                @if($setting->facebook)
                <li><a href="{{$setting->facebook}}" title="facebook" target="_blank" class="fa fa-facebook"></a></li>
                @endif

                @if($setting->twitter)
                <li><a href="{{$setting->twitter}}"  title="twitter" target="_blank" class="fa fa-twitter"></a></li>
                @endif

                @if($setting->pinterest)
                <li><a href="{{$setting->pinterest}}" title="pinterest" target="_blank" class="fa fa-pinterest"></a></li>
                @endif

                @if($setting->instagram)
                <li><a href="{{$setting->instagram}}" title="instagram" target="_blank" class="fa fa-instagram"></a></li>
                @endif

                @if($setting->linkedin)
                <li><a href="{{$setting->linkedin}}" title="linkedin" target="_blank" class="fa fa-linkedin-square"></a></li>
                @endif
            </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!------ Who We Are End ------> 

@endsection