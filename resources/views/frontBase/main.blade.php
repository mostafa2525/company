@include('front.inc._linksHeader')
@include('front.inc._header')

@yield('content')

@include('front.inc._footer')
@include('front.inc._linksFooter')